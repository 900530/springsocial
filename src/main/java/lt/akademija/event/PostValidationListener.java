package lt.akademija.event;

import javax.faces.component.UIInput;
import javax.faces.event.AbortProcessingException;
import javax.faces.event.PhaseEvent;
import javax.faces.event.PhaseId;
import javax.faces.event.PhaseListener;
import javax.faces.event.PostValidateEvent;
import javax.faces.event.SystemEvent;
import javax.faces.event.SystemEventListener;

public class PostValidationListener implements PhaseListener, SystemEventListener {

	private static final long serialVersionUID = -6430383442677911688L;

	@Override
	public boolean isListenerForSource(Object source) {
		if (source instanceof javax.faces.component.UIInput) {
			return true;
		}
		return false;
	}

	@Override
	public void processEvent(SystemEvent event) throws AbortProcessingException {
		UIInput source = (UIInput) event.getSource();
		String styleClass = (String) source.getAttributes().get("styleClass");
		if(styleClass == null || styleClass.isEmpty()) {
			styleClass = "";
		} else {
			styleClass += " ";
		}
        if(!source.isValid()) {
        	styleClass += "invalid";
        } else {
        	styleClass += "valid";
        }
        source.getAttributes().put("styleClass", styleClass);
	}

	@Override
	public void afterPhase(PhaseEvent event) {
		// Nothing to do here.
	}

	@Override
	public void beforePhase(PhaseEvent event) {
		event.getFacesContext().getViewRoot().subscribeToViewEvent(PostValidateEvent.class, new PostValidationListener());
	}

	@Override
	public PhaseId getPhaseId() {
		return PhaseId.PROCESS_VALIDATIONS;
	}

}
