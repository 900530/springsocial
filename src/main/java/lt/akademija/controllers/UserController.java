package lt.akademija.controllers;

import java.util.Date;
import java.util.List;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import lt.akademija.entities.User;
import lt.akademija.models.UserModel;
import lt.akademija.services.RoleService;
import lt.akademija.services.UserService;

public class UserController {
	
	private static final Logger logger = LogManager.getLogger(UserController.class.getName());

	private UserModel userModel;
	private UserService userService;
	private RoleService roleService;
	
	public String create(){
		User user = new User();
		user.setRole(roleService.getRole(new Integer(2)));
		user.setRegistration_date(new Date());
		userModel.setCurrentUser(user);
		return "addUser";
	}
	
	public String update(User user){
		userModel.setCurrentUser(user);
		save(user);
		logger.info("Profile: " + userModel.getCurrentUser().getLogin() + " was updated");
		return "user-update";
	}
	
	public String save(){
		userService.save(userModel.getCurrentUser());
		if(userModel.getCurrentUser().getId() == null){
			logger.info("New user: " + "'" + userModel.getCurrentUser().showFullName() + "'" + " was created by: " 
					+ "'" + userModel.getLoggedUser().showFullName() + "'");
		}
		else {
			logger.info("User: " + "'" + userModel.getCurrentUser().showFullName() + "'" + " was updated by: " 
					+ "'" + userModel.getLoggedUser().showFullName() + "'");
		}
		return "save";
	}
	
	/**
	 * Method check if the given user has administrator rights.
	 * @param user
	 * @return boolean value of true if user is administrator, false if not
	 */
	public boolean isAdmin(User user) {
		if (user != null) {
			if (user.getRole().getRole().toLowerCase().trim().equals("admin")) {
				return true;
			}
		}
		return false;
	}
	
	public void save(User user) {
		user.setRole(roleService.getRole(new Integer(user.getRole().getId())));
		userService.save(user);
	}
	
	public String cancel(){
		userModel.setCurrentUser(null);
		return "cancel";
	}
	
	public void delete(User user){
		userService.delete(user);
	}
	
	public String overview(User user){
		userModel.setCurrentUser(user);
		return "user-overview";
	}
	
	public User getUser(String login){
		return userService.getUser(login);
	}
	
	public List<User> autoComplete(String query) {
		List<User> userList = userService.findUsersByNamePattern(query);
		return userList;
	}

	/**
	 * This method checks users assigned to the project and
	 * compares to all users in the database. Returns list of users who are not assigned to the project.
	 * @param query Name and surname of user
	 * @return List<User> all users who are not in the project without duplicates.
	 */
	
	public String capitalizeFirstLetter(String str) {
		  if(str != null && !str.isEmpty()) {
		    return Character.toUpperCase(str.charAt(0)) + str.substring(1);
		  }
		  return str;
	}
	
	public String getFullName(User user) {
		if (user == null) {
			return "Unassigned";
		}
		return user.getName() + " " + user.getSurname();
	}
	
	public void showWhoLoggedOut(){
		logger.info("User: " + userModel.getLoggedUser().showFullName() + " logged out");
	}
	
	public UserModel getUserModel() {
		return userModel;
	}

	public void setUserModel(UserModel userModel) {
		this.userModel = userModel;
	}

	public UserService getUserService() {
		return userService;
	}

	public void setUserService(UserService userService) {
		this.userService = userService;
	}

	public RoleService getRoleService() {
		return roleService;
	}

	public void setRoleService(RoleService roleService) {
		this.roleService = roleService;
	}
	
}
