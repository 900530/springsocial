package lt.akademija.services;

import java.util.List;

import lt.akademija.entities.User;
import lt.akademija.repositories.UserDao;

public class UserService {
	private UserDao userDao;
	
	public User getUser(String login){
		return userDao.getUser(login);
	}
	
	public void save(User user){
		userDao.save(user);
	}
	
	public void delete(User user){
		userDao.delete(user);
	}
	
	public User findById(Integer id) {
		return userDao.findById(id);
	}
	
	public List<User> findAll(){
		return userDao.findAll();
	}
	
	public List<User> findUsersByNamePattern(String query) {
		return userDao.findUsersByNamePattern(query);
	}

	public UserDao getUserDao() {
		return userDao;
	}

	public void setUserDao(UserDao userDao) {
		this.userDao = userDao;
	}
	
}
