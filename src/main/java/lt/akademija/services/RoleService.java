package lt.akademija.services;

import java.util.List;

import lt.akademija.entities.Role;
import lt.akademija.repositories.RoleDao;

public class RoleService {
	private RoleDao roleDao;
	
	public Role getRole(Integer id){
		return roleDao.getRole(id);
	}
	
	public List<Role> findAll() {
		return roleDao.findAll();
	}

	public RoleDao getRoleDao() {
		return roleDao;
	}

	public void setRoleDao(RoleDao roleDao) {
		this.roleDao = roleDao;
	}
	
	
}
