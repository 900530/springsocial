package lt.akademija.security;

import java.io.Serializable;

import javax.faces.application.FacesMessage;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.LockedException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.context.SecurityContextHolder;

import lt.akademija.controllers.UserController;

public class LoginBean implements Serializable{
	
	private static final Logger logger = LogManager.getLogger(UserController.class.getName());
  
	private static final long serialVersionUID = -4587782587109330160L;
	
	private String userName = null; 
    private String password = null;

    private AuthenticationManager authenticationManager = null;
    
    public String login() {
        try {
            Authentication request = new UsernamePasswordAuthenticationToken(this.getUserName(), this.getPassword());
            Authentication result = authenticationManager.authenticate(request);
    	    SecurityContextHolder.getContext().setAuthentication(result);

        } 	catch (LockedException e){
			logger.warn("Authentication failed: " + e.getMessage());
        	FacesContext facesContext = FacesContext.getCurrentInstance();
    		facesContext.addMessage("loginInfo", new FacesMessage(FacesMessage.SEVERITY_ERROR, 
    				"Your account has been deactivated.", ""));
            return null;
    	}	catch (AuthenticationException e) {
    		logger.warn("Authentication failed: " + e.getMessage());
            FacesContext facesContext = FacesContext.getCurrentInstance();
    		facesContext.addMessage("loginInfo", new FacesMessage(FacesMessage.SEVERITY_ERROR, 
    				"Login or password incorrect.", ""));
            return null;
        } 
        logger.info("Successfully authenticated. Security context contains: " + 
        		SecurityContextHolder.getContext().getAuthentication());
        return "correct";
    }

    public String cancel() {
        return null;
    }

    public String logout(){
    	ExternalContext ec = FacesContext.getCurrentInstance().getExternalContext();
		ec.getSessionMap().remove("tabController"); // Destroying tab controller bean to reset tab index values for a new session
		ec.getSessionMap().remove("sprintController");
        SecurityContextHolder.clearContext();
        return "loggedout";
    }
 
    public AuthenticationManager getAuthenticationManager() {
        return authenticationManager;
    }

    public void setAuthenticationManager(AuthenticationManager authenticationManager) {
        this.authenticationManager = authenticationManager;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
 
    
}