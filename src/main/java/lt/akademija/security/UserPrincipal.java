package lt.akademija.security;

import java.util.Collection;

import org.springframework.security.core.GrantedAuthority;

import lt.akademija.entities.User;

public class UserPrincipal extends org.springframework.security.core.userdetails.User{
	
	private static final long serialVersionUID = -4271897545531624410L;

	private User user;
	
	public UserPrincipal(
			String username,
			String password,
			boolean enabled,
			boolean accountNonExpired,
			boolean credentialsNonExpired,
			boolean accountNonLocked,
			Collection<? extends GrantedAuthority> authorities,
			User user) {
		
		super(username,
				password,
				enabled,
				accountNonExpired,
				credentialsNonExpired,
				accountNonLocked,
				authorities);
		this.user = user;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}
	
}
