package lt.akademija.models;

import org.springframework.security.core.context.SecurityContextHolder;

import lt.akademija.entities.User;
import lt.akademija.security.UserPrincipal;

public class UserModel {

	private User currentUser;

	public User getLoggedUser() {
		Object principal = SecurityContextHolder.getContext().getAuthentication().getPrincipal();
		User user = ((UserPrincipal) principal).getUser();
		return user;
	}
		
	public User getCurrentUser() {
		return currentUser;
	}

	public void setCurrentUser(User currentUser) {
		this.currentUser = currentUser;
	}



}
