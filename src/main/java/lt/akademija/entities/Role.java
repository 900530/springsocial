package lt.akademija.entities;

import java.io.Serializable;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.OneToMany;

@Entity
public class Role implements Serializable{
	private static final long serialVersionUID = 5251185924942990178L;

	@Id
    @GeneratedValue(strategy=GenerationType.AUTO)
	@Column(name="ID")
    private Integer id;
   
	@Column(name="ROLE", length=32)
    private String role;
   
    @OneToMany(cascade=CascadeType.ALL)
    @JoinTable(name="USER_ROLES",
        joinColumns = {@JoinColumn(name="ROLE_ID", referencedColumnName="ID")},
        inverseJoinColumns = {@JoinColumn(name="USER_ID", referencedColumnName="ID")}
    )
    private Set<User> userRoles;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getRole() {
		return role;
	}

	public void setRole(String role) {
		this.role = role;
	}

	public Set<User> getUserRoles() {
		return userRoles;
	}

	public void setUserRoles(Set<User> userRoles) {
		this.userRoles = userRoles;
	}
    	
}
