package lt.akademija.entities;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.OneToOne;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;

import lt.akademija.security.PasswordEncoder;

@Entity
public class User implements Serializable{
	private static final long serialVersionUID = 5621481353510942069L;

	@Id
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    private Integer id;
	
	@NotNull
	@Size(min=2, max=32)
	@Column(unique=true, length=32)
	@Pattern(regexp="^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$", message="This is not a valid email.")
	private String login;
	
	@NotNull
	private String password;
	
	private Date registration_date;
	
	@Size(min=2, max=32, message="First name must be between 2 and 32 characters.")
	@Column(length=32)
	private String name;
	
	@Size(min=2, max=32, message="Last name must be between 2 and 32 characters.")
	@Column(length=32)
	private String surname;
	
	private boolean active;
	
    @OneToOne(fetch = FetchType.EAGER)
    @JoinTable(name="USER_ROLES",
        joinColumns = {@JoinColumn(name="USER_ID", referencedColumnName="ID")},
        inverseJoinColumns = {@JoinColumn(name="ROLE_ID", referencedColumnName="ID")}
    )
    private Role role;
    
	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getLogin() {
		return login;
	}

	public void setLogin(String login) {
		this.login = login;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		String hashedPassword = PasswordEncoder.encodePassword(password);
		this.password = hashedPassword;
	}

	public Role getRole() {
		return role;
	}

	public void setRole(Role role) {
		this.role = role;
	}

	public Date getRegistration_date() {
		return registration_date;
	}

	public void setRegistration_date(Date registration_date) {
		this.registration_date = registration_date;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getSurname() {
		return surname;
	}

	public void setSurname(String surname) {
		this.surname = surname;
	}

	public boolean isActive() {
		return active;
	}

	public void setActive(boolean active) {
		this.active = active;
	}
	
	public String showFullName() {
		return name + " " + surname;
	}
	
}
