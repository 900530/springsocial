package lt.akademija.beans;

import java.io.Serializable;
import java.util.Map;
import java.util.Properties;
import javax.faces.bean.SessionScoped;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.inject.Named;
import javax.servlet.http.HttpServletRequest;

import org.brickred.socialauth.AuthProvider;
import org.brickred.socialauth.Profile;
import org.brickred.socialauth.SocialAuthConfig;
import org.brickred.socialauth.SocialAuthManager;
import org.brickred.socialauth.util.SocialAuthUtil;

@Named("userSession")
@SessionScoped
public class UserSessionBean implements Serializable {

	private static final long serialVersionUID = 3247665086155942392L;
	private SocialAuthManager manager;
    private String            originalURL;
    private String            providerID;
    private Profile           profile;
    
    public UserSessionBean() {}

	public void socialConnect() throws Exception {
        // Put your keys and secrets from the providers here 
        Properties props = System.getProperties();
        props.put("graph.facebook.com.consumer_key", "533750156810065");
        props.put("graph.facebook.com.consumer_secret", "c5bc27ab3fd024656c9165430f3939ff");
        // Define your custom permission if needed
        props.put("graph.facebook.com.custom_permissions", "email,user_birthday,user_location");
        
        // Initiate required components
        SocialAuthConfig config = new SocialAuthConfig();
        config.load(props);
        manager = new SocialAuthManager();
        manager.setSocialAuthConfig(config);
        
        // 'successURL' is the page you'll be redirected to on successful login
        //ExternalContext externalContext   = FacesContext.getCurrentInstance().getExternalContext();
        String          successURL        = "http://localhost:8080/SpringSocial/socialLoginSuccess.xhtml"; 
        String          authenticationURL = manager.getAuthenticationUrl(providerID, successURL);
        FacesContext.getCurrentInstance().getExternalContext().redirect(authenticationURL);
    }
    
    public void pullUserInfo() {
        try {
            // Pull user's data from the provider
            ExternalContext    externalContext = FacesContext.getCurrentInstance().getExternalContext();
            HttpServletRequest request         = (HttpServletRequest) externalContext.getRequest();
            Map<String, String> map            = SocialAuthUtil.getRequestParametersMap(request);
            if (this.manager != null) {
                AuthProvider provider = manager.connect(map);
                this.profile = provider.getUserProfile();
                
                // Do what you want with the data (e.g. persist to the database, etc.)
                System.out.println("User's Social profile: " + profile.getFirstName());
            
                // Redirect the user back to where they have been before logging in
                FacesContext.getCurrentInstance().getExternalContext().redirect(originalURL);
            
            } else FacesContext.getCurrentInstance().getExternalContext().redirect("http://localhost:8080/SpringSocial/user/home.xhtml");
        } catch (Exception ex) {
            System.out.println("UserSession - Exception: " + ex.toString());
        } 
    }
    
//    public void logOut() {
//        try {
//            // Disconnect from the provider
//            manager.disconnectProvider(providerID);
//            
//            // Invalidate session
//            ExternalContext    externalContext = FacesContext.getCurrentInstance().getExternalContext();
//            HttpServletRequest request         = (HttpServletRequest) externalContext.getRequest();
//            this.invalidateSession(request);
//            
//            // Redirect to home page
//            FacesContext.getCurrentInstance().getExternalContext().redirect(externalContext.getRequestContextPath() + "home.xhtml");
//        } catch (IOException ex) {
//            System.out.println("UserSessionBean - IOException: " + ex.toString());
//        }
//    }

	public SocialAuthManager getManager() {
		return manager;
	}

	public void setManager(SocialAuthManager manager) {
		this.manager = manager;
	}

	public String getOriginalURL() {
		return originalURL;
	}

	public void setOriginalURL(String originalURL) {
		this.originalURL = originalURL;
	}

	public String getProviderID() {
		return providerID;
	}

	public void setProviderID(String providerID) {
		this.providerID = providerID;
	}

	public Profile getProfile() {
		return profile;
	}

	public void setProfile(Profile profile) {
		this.profile = profile;
	}
    
    // Getters and Setters
}