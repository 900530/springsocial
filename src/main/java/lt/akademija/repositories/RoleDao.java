package lt.akademija.repositories;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.TypedQuery;

import lt.akademija.entities.Role;

public class RoleDao {
	
	private EntityManagerFactory entityManagerFactory;
	private EntityManager em;
	
	public void setEntityManagerFactory(EntityManagerFactory entityManagerFactory) {
		this.entityManagerFactory = entityManagerFactory;
		this.em = entityManagerFactory.createEntityManager();
	}
	
    public Role getRole(Integer id) {
    	List<Role> roleList = new ArrayList<Role>();
    	em = entityManagerFactory.createEntityManager();
		TypedQuery<Role> authorQuery = em.createQuery("SELECT r From Role r WHERE r.id = :id", Role.class);
		authorQuery.setParameter("id", id);
		roleList = authorQuery.getResultList();
        if (roleList.size() > 0)
            return roleList.get(0);
        else
            return null;
    }

	public List<Role> findAll() {
		EntityManager em = entityManagerFactory.createEntityManager();
		try {
			TypedQuery<Role> q = em.createQuery("SELECT r FROM Role r", Role.class);
			return q.getResultList();
		} finally {
			em.close();
		}
	}
}
