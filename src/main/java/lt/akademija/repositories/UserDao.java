package lt.akademija.repositories;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

import lt.akademija.entities.User;

public class UserDao {

	private EntityManagerFactory entityManagerFactory;
	private EntityManager em;

	public void setEntityManagerFactory(EntityManagerFactory entityManagerFactory) {
		this.entityManagerFactory = entityManagerFactory;
		this.em = entityManagerFactory.createEntityManager();
	}

	public User getUser(String login) {
		List<User> userList = new ArrayList<User>();
		em = entityManagerFactory.createEntityManager();
		TypedQuery<User> authorQuery = em.createQuery("SELECT u From User u WHERE u.login = :login", User.class);
		authorQuery.setParameter("login", login);
		userList = authorQuery.getResultList();
		if (userList.size() > 0)
			return userList.get(0);
		else
			return null;
	}

	public void save(User user) {
		em = entityManagerFactory.createEntityManager();
		try {
			em.getTransaction().begin();
			if (!em.contains(user))
				user = em.merge(user);
			em.persist(user);
			em.getTransaction().commit();
		} 
		finally {
			em.close();
		}
	}
	
	public void delete(User user) {
		em = entityManagerFactory.createEntityManager();
		em.getTransaction().begin();
		user = em.merge(user);
		em.remove(user);
		em.getTransaction().commit();
		em.close();
	}

	public List<User> findAll() {
		em = entityManagerFactory.createEntityManager();
		try {
			CriteriaBuilder cb = em.getCriteriaBuilder();
			CriteriaQuery<User> cq = cb.createQuery(User.class);
			Root<User> root = cq.from(User.class);
			cq.select(root);
			TypedQuery<User> q = em.createQuery(cq);
			return q.getResultList();
		} finally {
			em.close();
		}
	}

	public List<User> findUsersByNamePattern(String query) {
		em = entityManagerFactory.createEntityManager();
		try {
			TypedQuery<User> q = em.createQuery(
					"SELECT u FROM User u WHERE LOWER(CONCAT(u.name, u.surname)) LIKE :pattern", User.class);
			q.setParameter("pattern", "%" + query.toLowerCase().replaceAll("\\s+","") + "%");
			return q.getResultList();
		} finally {
			em.close();
		}
	}

	public User findById(Integer id) {
		em = entityManagerFactory.createEntityManager();
		try {
			return em.find(User.class, id);
		} finally {
			em.close();
		}
	}

}
